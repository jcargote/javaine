package gob.ine.cognos.listas.principal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import gob.ine.cognos.listas.objetos.Producto;

public class principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char respuesta;
		/*
		 * String nombre; int precio,cantidad;
		 */
		List<Producto> productos = new ArrayList<Producto>();
		do {
			// Producto producto = new Producto();
			System.out.println("Ingrese los datos del Producto");
			String nombre = sc.nextLine();
			// producto.setNombre(nombre);
			System.out.println("Ingrese Precio del Producto");
			int precio = sc.nextInt();
			// producto.setPrecio(precio);
			System.out.println("Ingrese cantidad del Producto");
			int cantidad = sc.nextInt();
			// producto.setCantidad(cantidad);

			Producto producto = new Producto(nombre, precio, cantidad);
			productos.add(producto);
			System.out.println("Desea seguir adicionando productos? S/N");
			String cad = sc.nextLine();
			cad = sc.nextLine();
			respuesta = cad.charAt(0);

		} while (respuesta == 'S');

		System.out.println("La Lista de productos es ");
		listarProductos(productos);

		System.out.println("Desea eliminar productos? S/N");
		String cad = sc.nextLine();
		respuesta = cad.charAt(0);
//remover item
		while (respuesta == 'S') {
			System.out.println("Ingrese el nro de Producto a eliminar");
			int borraProducto = sc.nextInt();
			productos.remove(borraProducto - 1);

			System.out.println("Desea eliminar productos? S/N");
			cad = sc.nextLine();
			if (cad.length() == 0)
				cad = sc.nextLine();
			respuesta = cad.charAt(0);
		}

		listarProductos(productos);
		System.out.println("TOTAL " + precioTotal((ArrayList<Producto>) productos));
	}

	private static void listarProductos(List<Producto> productos) {
		int c = 1;
		System.out.println("N \t Producto  \t Precio \t Cantidad");
		for (Producto producto : productos) {
			System.out.println(c + "\t" + producto.getNombre() + "\t" + producto.getPrecio() + "\t"
					+ producto.getPrecio() + "\t" + producto.getCantidad());
			c++;
		}
	}

	private static double precioTotal(ArrayList<Producto> productos) {
		double res = 0;
		for (Producto producto : productos) {
			res = res + (producto.getPrecio() * producto.getCantidad());
		}
		return res;
	}

}
