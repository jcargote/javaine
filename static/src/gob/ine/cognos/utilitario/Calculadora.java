package gob.ine.cognos.utilitario;

public class Calculadora {
	
	private final static double VALOR_MINIMO = -100000;

	public static double calcularPromedio(double[] numeros) {
		int cont=0;
		double a=0,prom;
		for(int i=0; i<numeros.length;i++)
		{
			a=a+numeros[i];
			cont++;
		}
		prom=a/cont;
		return prom;
	}

	public static double obtenerMayor(double[] numeros2) {
	    double mayor;
	    mayor= VALOR_MINIMO;
		//mayor=numeros2[0];
		for(int i=1; i<numeros2.length;i++)
		{
			if (mayor<numeros2[i])
			{
				mayor = numeros2[i];
			}
			
		}
		
		return mayor;
		
	}
	
}
