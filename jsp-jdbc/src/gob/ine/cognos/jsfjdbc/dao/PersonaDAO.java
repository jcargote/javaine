package gob.ine.cognos.jsfjdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gob.ine.cognos.jsfjdbc.model.Persona;

public class PersonaDAO {

	private static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/curso?currentSchema=curso_java",
					"postgres", "postgres");
		} catch (ClassNotFoundException e) {
			System.out.println("Error al cargar Driver " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("no se pudo abrir la conneccion " + e.getMessage());
		}
		return con;
	}

	public static List<Persona> listar() {
		List<Persona> personas = new ArrayList<Persona>();
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		if (con != null) {
			String sql = "SELECT * FROM persona";
			// PreparedStatement ps;
			try {
				ps = con.prepareStatement(sql);
				// ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Persona persona = new Persona();
					persona.setId(rs.getInt("id"));
					persona.setNombre(rs.getString("nombre"));
					persona.setEmail(rs.getString("email"));
					persona.setGenero(rs.getString("genero"));
					persona.setEdad(rs.getInt("edad"));
					personas.add(persona);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("La consulta no pudo ejecutarse " + e.getMessage());
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						System.out.println("Error al cerrar la base"+ e.getMessage());
					}
				if (ps != null)
					try {
						ps.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						System.out.println("Error al cerrar la base"+ e.getMessage());
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la base"+ e.getMessage());
					}
			}
		} else {
			System.out.println("No se pudo abrir la coneccion a la base de datos");
		}
		return personas;
	}

	/*public static int guardar(Persona persona) {
		//Connection
	}*/

}
