
public abstract class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double resultado;
		Cuadrado cuadrado = new Cuadrado(20);
		cuadrado.area();
		resultado=cuadrado.getareaCuadrado();
        System.out.println("Área del cuadrado "+ cuadrado.getLado() + ":" +resultado);
        
        Circulo circulo = new Circulo(200);
        circulo.area();
        resultado=circulo.getAreaRadio();
        System.out.println("Área del círculo con radio "+ circulo.getRadio()  +" : " + resultado);
        
        Rectangulo rectangulo = new Rectangulo(100, 40);
        rectangulo.area();
        resultado=rectangulo.getAreaRectangulo();
        System.out.println("Área del rectángulo con base "+ rectangulo.getBase()  +" y altura "+ rectangulo.getAltura() + " es :" + resultado);

	}

}
