package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FlowLayoutEjemplo extends JFrame {

	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;

	public FlowLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(100, 100);
		JPanel panelPrincipal = (JPanel) getContentPane();
		panelPrincipal.setBackground(Color.BLUE);
		panelPrincipal.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jbBoton1 = new JButton("Boton 1");
		jbBoton2 = new JButton("Boton 2");
		jbBoton3 = new JButton("Boton 3");
		panelPrincipal.add(jbBoton1);
		panelPrincipal.add(jbBoton2);
		panelPrincipal.add(jbBoton3);
		pack();
	}

	public static void main(String[] args) {
		FlowLayoutEjemplo ejemplo = new FlowLayoutEjemplo();
		ejemplo.setVisible(true);
	}

}
