package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutEjemplo extends JFrame {
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	private JButton jbBoton4;

	public GridLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PanelPersonal panelPersonal = new PanelPersonal();
		panelPersonal.setBackground(Color.CYAN);
		setContentPane(panelPersonal);

		JPanel panelPrincipal = (JPanel) getContentPane();
		panelPrincipal.setLayout(new GridLayout(2, 2));
		jbBoton1 = new JButton("Boton 1");
		jbBoton2 = new JButton("Boton 2");
		jbBoton3 = new JButton("Boton 3");
		jbBoton4 = new JButton("Boton 4");
		panelPrincipal.add(jbBoton1);
		panelPrincipal.add(jbBoton2);
		panelPrincipal.add(jbBoton3,0);
		panelPrincipal.add(jbBoton4);
		pack();
	}

	public static void main(String[] args) {
		GridLayoutEjemplo ejemplo = new GridLayoutEjemplo();
		ejemplo.setVisible(true);
	}
}
