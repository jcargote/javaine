package gob.ine.cognos.swinginicio.principal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BorderLayoutEjemplo extends JFrame {
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	private JButton jbBoton4;
	private JButton jbBoton5;
	
	public BorderLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PanelPersonal panelPersonal = new PanelPersonal();
		//panelPersonal.setBackground(Color.CYAN);
		setContentPane(panelPersonal);
		
		JPanel panelPrincipal = (JPanel) getContentPane();
		panelPrincipal.setLayout(new BorderLayout(4, 2));
		jbBoton1 = new JButton("Boton 1");
		jbBoton2 = new JButton("Boton 2");
		jbBoton3 = new JButton("Boton 3");
		jbBoton4 = new JButton("Boton 4");
		jbBoton5 = new JButton("Boton 5");
		panelPrincipal.add(jbBoton1,BorderLayout.PAGE_START);
		panelPrincipal.add(jbBoton2,BorderLayout.LINE_START);
		panelPrincipal.add(jbBoton3,BorderLayout.LINE_END);
		panelPrincipal.add(jbBoton4,BorderLayout.CENTER);
		panelPrincipal.add(jbBoton5,BorderLayout.PAGE_END);
		pack();
	}
	
	public static void main(String[] args) {
		BorderLayoutEjemplo ejemplo = new BorderLayoutEjemplo();
		ejemplo.setVisible(true);
	}
}
