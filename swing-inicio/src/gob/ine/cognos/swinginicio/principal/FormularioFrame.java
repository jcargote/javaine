package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class FormularioFrame extends JFrame {

	private JLabel jlVacio;
	private JLabel jlLogin;
	private JLabel jlPassword;
	private JTextField jtLogin;
	// private JTextField jtPassword;
	private JButton jbIngresar;
	private JPasswordField jpPassword;

	public FormularioFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(100, 100);
		JPanel panelPrincipal = (JPanel) getContentPane();
		// panelPrincipal.setBackground(Color.BLUE);
		panelPrincipal.setLayout(new GridLayout(3, 2));
		jlLogin = new JLabel("Login: ");
		jlVacio = new JLabel("");
		jlPassword = new JLabel("Password: ");
		jtLogin = new JTextField(15);
		// jtPassword = new JTextField(15);
		jpPassword = new JPasswordField();
		jbIngresar = new JButton("INGRESAR");
		panelPrincipal.add(jlLogin);
		panelPrincipal.add(jtLogin);
		panelPrincipal.add(jlPassword);
		panelPrincipal.add(jpPassword);
		panelPrincipal.add(jlVacio);
		panelPrincipal.add(jbIngresar);

		jlLogin.setBackground(Color.CYAN);
		jlLogin.setText("Usuario: ");
		//jtLogin.setText("Juan");

		jbIngresar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// JOptionPane.showMessageDialog(null, "Click Boton");
				String login = jtLogin.getText();
				String password = new String (jpPassword.getPassword());
				if (login.equalsIgnoreCase("Juan") && password.equalsIgnoreCase("12345678")) {
					//JOptionPane.showMessageDialog(null, "Bienvenido " + login);
				ContenidoFrame contenidoFrame= new ContenidoFrame();
				contenidoFrame.setVisible(true);
			}
				else
					JOptionPane.showMessageDialog(null, "Error de autenticacion ");

			}
		});

		// );

		pack();
	}

	public static void main(String[] args) {
		FormularioFrame ejemplo = new FormularioFrame();
		ejemplo.setVisible(true);
	}
}