package gob.ine.cognos.collections.principal;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.LinkedList;

public class Principal {

	public static void main(String[] args) {
		List cadenas = new ArrayList();

		List<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("Pedro", "Perez"));
		personas.add(new Persona("Pedro2", "Perez"));
		personas.add(new Persona("Pedro3", "Perez"));
		personas.add(new Persona("Pedro4", "Perez"));
		personas.add(new Persona("Pedro5", "Perez"));
		Persona personaSergio = new Persona();
		personaSergio.setNombre("Sergio");
		personaSergio.setApellido("Perez");
		personaSergio.setEdad(74);
		personas.add(personaSergio);
		// personas.set(2,personaSergio);

		for (int i = 0; i < personas.size(); i++) {
			Persona personaItem = personas.get(i);
			System.out.println(personas.get(i));
			System.out.println("Nombre ->" + personas.get(i).getNombre());
		}
		System.out.println();

		if (personas.contains(personaSergio))
			System.out.println("Sergio esta");

		personas.remove(5);

		for (Persona personaItem : personas) {
			System.out.println(personaItem);
			System.out.println("EDAD ->" + personaItem.getEdad());
		}

		if (personas.contains(personaSergio))
			System.out.println("Sergio esta");
		else
			System.out.println("Sergio no esta");

		//List<String> cadenasLinked = new LinkedList<String>();
		Queue<String> cadenasLinked = new LinkedList<String>();

		cadenasLinked.add("Prueba1");
		cadenasLinked.add("Prueba2");
		cadenasLinked.add("Prueba3");

		for (String cadena : cadenasLinked) {
			System.out.println(cadena);
		}
		
	}

}
