package gob.ine.cognos.sets.principal;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import gob.ine.cognos.sets.objetos.Producto;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char respuesta;

		HashSet<Producto> productos = new HashSet<Producto>();
		do {
			System.out.println("Ingrese los datos del Producto");
			String nombre = sc.nextLine();

			System.out.println("Ingrese Precio del Producto");
			int precio = sc.nextInt();

			System.out.println("Ingrese cantidad del Producto");
			int cantidad = sc.nextInt();

			Producto producto = new Producto(nombre, precio, cantidad);
			productos.add(producto);
			System.out.println("Desea seguir adicionando productos? S/N");
			String cad = sc.nextLine();
			cad = sc.nextLine();
			respuesta = cad.charAt(0);

		} while (respuesta == 'S');

		System.out.println("La Lista de productos es ");
		listarProductos(productos);

		System.out.println("Desea eliminar productos? S/N");
		String cad = sc.nextLine();
		respuesta = cad.charAt(0);

		while (respuesta == 'S') {
			System.out.println("Ingrese el nro de Producto a eliminar");
			int borraProducto = sc.nextInt();
			productos.remove(borraProducto-1);

			System.out.println("Desea eliminar productos? S/N");
			cad = sc.nextLine();
			if (cad.length() == 0)
				cad = sc.nextLine();
			respuesta = cad.charAt(0);
		}

		listarProductos(productos);
		System.out.println("TOTAL " + precioTotal((HashSet<Producto>) productos));
	}

	private static void listarProductos(HashSet<Producto> productos) {
		int c = 1;
		System.out.println("N \t Producto  \t Precio \t Cantidad");
		for (Producto producto : productos) {
			System.out.println(c + "\t" + producto.getNombre() + "\t" + producto.getPrecio() + "\t"
					+ producto.getPrecio() + "\t" + producto.getCantidad());
			c++;
		}
	}

	private static double precioTotal(HashSet<Producto> productos) {
		double res = 0;
		for (Producto producto : productos) {
			res = res + (producto.getPrecio() * producto.getCantidad());
		}
		return res;
	}

}
