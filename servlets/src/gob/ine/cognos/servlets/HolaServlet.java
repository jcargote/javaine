package gob.ine.cognos.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hola")
public class HolaServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		System.out.println("Iniciando HolaServlet");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("llamado get");
		String nombre = req.getParameter("nombre");
		PrintWriter out = resp.getWriter();
		// out.print("<html><head><body><h1>hola " + nombre
		// +"</h1></body></head></html>");
		out.println("<HTML><HEAD><TITLE>Leyendo parámetros</TITLE></HEAD>");
		out.println("<BODY BGCOLOR=\"#CCBBAA\">");
		out.println("<H2>Leyendo parámetros desde un formulario html</H2><P>");
		out.println("<UL>\n");
		out.println("Te llamas " + req.getParameter("NOM") + "<BR>");
		out.println("y tienes " + req.getParameter("EDA") + " años<BR>");
		out.println("</BODY></HTML>");
		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("llamado posts");
		String nombre = req.getParameter("nombre");
		PrintWriter out = resp.getWriter();
		// out.print("<html><head><body><h1>hola " + nombre
		// +"</h1></body></head></html>");
		out.println("<HTML><HEAD><TITLE>Leyendo parámetros</TITLE></HEAD>");
		out.println("<BODY BGCOLOR=\"#HHBBDD\">");
		out.println("<H2>Leyendo parámetros desde un formulario html</H2><P>");
		out.println("<UL>\n");
		out.println("Te llamas " + req.getParameter("NOM") + "<BR>");
		out.println("y tienes " + req.getParameter("EDA") + " años<BR>");
		out.println("</BODY></HTML>");
		out.close();
	}
}
