package gob.ine.cognos.excepciones.principal;

import java.util.Scanner;

import gob.ine.cognos.excepciones.objetos.ValidacionException;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nombre;
		String apellido;
		int edad;

		nombre = sc.nextLine();
		apellido = sc.nextLine();
		edad = sc.nextInt();

		try {
			if (validarCadena(nombre)) {
				System.out.println("Nombre correcto");
			}
		} catch (ValidacionException e) {
			System.out.println("Error en la validacion de nombre");
			System.out.println(e.getCodError() + " - " + e.getMessage());
		}

		try {
			if (validarCadena(apellido)) {
				System.out.println("Apellido correcto");
			}
		} catch (ValidacionException e) {
			System.out.println("Error en la validacion de apellido");
		}

		try {
			if (validarEntero(edad)) {
				System.out.println("Edad correcta");
			}
		} catch (ValidacionException e) {
			System.out.println("Error en la validacion de edad");
		}

	}

	private static boolean validarCadena(String cadena) throws ValidacionException {
		if (cadena.length() < 3 || cadena.length() > 15)
			throw new ValidacionException("Error validacion cadena", 255);
		else
			return true;
	}

	private static boolean validarEntero(int numero) throws ValidacionException {
		if (numero < 0 || numero > 150)
			throw new ValidacionException();
		else
			return true;
	}

}
