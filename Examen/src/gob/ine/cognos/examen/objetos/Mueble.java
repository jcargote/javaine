package gob.ine.cognos.examen.objetos;

public abstract class Mueble {
	  protected String codigo;
	    protected String tipo_madera;
	    protected double precio;
	    
	    public Mueble(){    	
	    }
	    
	    public Mueble(String codigo,String tipo_madera, double precio) {
	    	this.codigo=codigo;
	    	this.tipo_madera=tipo_madera;
	    	this.precio=precio;	    	
	    }

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getTipo_madera() {
			return tipo_madera;
		}

		public void setTipo_madera(String tipo_madera) {
			this.tipo_madera = tipo_madera;
		}

		public double getPrecio() {
			return precio;
		}

		public void setPrecio(double precio) {
			this.precio = precio;
		}
	    
}
