package gob.ine.cognos.cuenta;

import gob.ine.cognos.cuenta.Cuenta;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cuenta cuenta= new Cuenta();
		cuenta.setNombreTitular("JOSE PEREZ");
		cuenta.setNroCuenta(20034005);
		System.out.println("TITULAR: =" + cuenta.getNombreTitular() + " NRO CUENTA: " + cuenta.getNroCuenta());
		cuenta.Modificar("LUIS GOMEZ");
		System.out.println("TITULAR: =" + cuenta.getNombreTitular() + " NRO CUENTA: " + cuenta.getNroCuenta());
		cuenta.Modificar(222333444);
		System.out.println("TITULAR: =" + cuenta.getNombreTitular() + " NRO CUENTA: " + cuenta.getNroCuenta());
		cuenta.Modificar(55566677, "PEDRO INTIMAYTA");
		System.out.println("TITULAR: =" + cuenta.getNombreTitular() + " NRO CUENTA: " + cuenta.getNroCuenta());

	}

}
