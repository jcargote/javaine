package gob.ine.cognos.cuenta;

public class Cuenta {

	private String nombreTitular;
	private int nroCuenta;
	
	public Cuenta() {
		
	}
	
	public Cuenta(String nombreTitular, int nroCuenta) {
		this.nombreTitular = nombreTitular;
		this.nroCuenta = nroCuenta;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public void Modificar(int nrocuenta, String nombreTitular)
	{
		this.nombreTitular= nombreTitular;
		this.nroCuenta= nrocuenta;
	}
	
	public void Modificar(int nroCuenta)
	{
		this.nroCuenta=nroCuenta;
	}
	
	public void Modificar(String nombreTitular)
	{
		this.nombreTitular=nombreTitular;
	}
	
}
