package gob.ine.cognos.archivos.principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class principal {

	public static void main(String[] args) {
		File archivo = new File("/home/java/prueba.txt");
		if (archivo.exists())
			System.out.println("Archivo existe");
		else {
			System.out.println("Archivo no existe");
			try {
				archivo.createNewFile();
				System.out.println("Archivo Creado");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error al crear archivo");
			}
		}
		try {
			FileReader fr= new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			String cadena;
			while((cadena= br.readLine()) != null)
				System.out.println(cadena);
		} catch (FileNotFoundException e) {
			System.out.println("Archivo no encontrado");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error al leer el archivo");
		}
	}

}
