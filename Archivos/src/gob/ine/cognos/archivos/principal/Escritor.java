package gob.ine.cognos.archivos.principal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Escritor {
	public static void main(String[] args) {
		File archivo= new File("/home/java/archivoescrito.txt");
		if (archivo.exists())
			System.out.println("Archivo existe");
		else {
			System.out.println("Archivo no existe");
			try {
				archivo.createNewFile();
				System.out.println("Archivo Creado");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error al crear archivo");
			}
		}
		
		try {
			FileWriter fw= new FileWriter(archivo);
			PrintWriter pw= new PrintWriter(fw);
			pw.write("Texto prueba\n");
			pw.write("Segundo Texto prueba");
			pw.println();
			pw.format("Hola %s edad %d",  "Roberto",40);
			pw.println();
			pw.format("Numero decima %.4f", 45.896547);
			System.out.println("TextoEscrito");
			fw.close();
		} catch (IOException e) {
			System.out.println("El archivo no puede ser leido");		}
	}
}
