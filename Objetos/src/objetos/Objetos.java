/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author JAVA
 */
public class Objetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Alumno alumno1= new Alumno("Pedro","Intimayta",38,'M');
//        alumno1.nombre="Pedro";
//        alumno1.apellido="Intimayta";
//        alumno1.genero='M';
//        alumno1.edad=30;
        System.out.println(alumno1.getNombre() + " " + alumno1.getApellido() + " - " + alumno1.getEdad() + " - " + alumno1.getGenero());
        alumno1.estudiar();
        alumno1.estudiar("Matematicas");
        Profesor profesor= new Profesor();
        profesor.setNombre("Sergio");
        profesor.setApellido("Martinez");
        
        alumno1.asistirAClases();
        profesor.asistirAClases();

        alumno1.estudiar();
        profesor.aplazar(alumno1);
        
        alumno1.irAlrecreo("Mañana");
        profesor.irAlrecreo("mañana");
        
        profesor.pagarImpuestos();
        profesor.pagarImpuestos(100);
        
        Funcionario funcionario= new Profesor();
        //funcionario.pagarImpuestos();
        
        Funcionario funcionarioDirector = new Director();
        //funcionarioDirector.pagarImpuestos();
        
        Funcionario funcionarioAlcaldia= new Funcionario() {
            @Override
            public void pagarImpuestos() {
                System.out.println("no quiere pagar impuestos");
            }

            @Override
            public void pagarImpuestos(double monto) {
                System.out.println("Pagando "+ monto + "impuestos");
            }
        };
                
        pagarImpuestosDeFuncionarios(funcionario);
        pagarImpuestosDeFuncionarios(funcionarioDirector);
        pagarImpuestosDeFuncionarios(funcionarioAlcaldia);

    }
    
    public static void pagarImpuestosDeFuncionarios(Funcionario funcionario){
        funcionario.pagarImpuestos();
    }
    
}
