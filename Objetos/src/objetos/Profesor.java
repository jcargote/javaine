/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author JAVA
 */
public class Profesor extends Persona implements Funcionario{
    private String matricula;

    public Profesor() {
    }

    public Profesor(String matricula,String nombre, String apellido, int edad, char genero) {
        super(nombre, apellido, edad, genero);
        this.matricula= matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    public void aplazar(){
        System.out.println(nombre + " Aplazar.....");        
    }

    public void aplazar(Alumno alumno){
        System.out.println(nombre + " Aplazando a " + alumno.nombre);        
    }

    @Override
    public void irAlrecreo(String turno) {
         System.out.println(nombre + " Tomar te en ....." + turno);   
    }

    @Override
    public void pagarImpuestos() {
        System.out.println("Pagando Impuestos");
    }

    @Override
    public void pagarImpuestos(double monto) {
        System.out.println("Pagando "+ monto + "Bs. de impuestos");
    }

}
