/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author JAVA
 */
public class Director implements Funcionario {

    @Override
    public void pagarImpuestos() {
        System.out.println("director pagando impustos");
    }

    @Override
    public void pagarImpuestos(double monto) {
        System.out.println("Director pagando " + monto + "Bs. de impuestos");
    }

}
