package gob.ine.cognos.javadoc.calculadora;

/**
 * Clase que sirve para realizar calculos
 * 
 * @author Juan Carlos Argote
 *
 */
public class Calculadora {

	/**
	 * Atributo para sumar el numero 1
	 */
	private int numero1;
	/**
	 * Atributo para sumar el numero 2
	 */
	private int numero2;
	
	public Calculadora() {
		
	}
	/**
	 * @param numero1  parametro numero 1 del constructor
	 * @param numero2 parametro numero 2 del constructor
	 */
	public Calculadora(int numero1, int numero2) {
		super();
		this.numero1 = numero1;
		this.numero2 = numero2;
	}
	public int getNumero1() {
		return numero1;
	}
	public void setNumero1(int numero1) {
		this.numero1 = numero1;
	}
	public int getNumero2() {
		return numero2;
	}
	public void setNumero2(int numero2) {
		this.numero2 = numero2;
	}
	
	/**
	 * @param numeroSumando numero sumando
	 * @param etiqueta valor descriptivo
	 * @return retorna valor entero que es la suma del sumando mas los otros dos numeros declarados
	 */
	public int calcular(int numeroSumando,String etiqueta) {
		System.out.println(etiqueta + numero1 + numero2 + numeroSumando);
		return numero1 + numero2 + numeroSumando;
		
	}
	
}
